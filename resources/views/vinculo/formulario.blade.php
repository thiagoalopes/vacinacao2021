@extends('layouts.template')

@section('content')


@include('messages.alert')

@if($vinculo->exists)

<div class="col-12 text-center">
    <h3>EDITAR VÍNCULO</h3>
</div>

<form action="{{route('vinculo.update', $vinculo)}}" method="post">
    <input type="hidden" name="_method" value="put">

    @else

    <div class="col-12 text-center">
        <h3>CADASTRO DE VÍNCULO</h3>
    </div>

    <form action="{{route('vinculo.store')}}" method="post">
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-12">
                <label>Nome *</label>
                <input name="descricao" id="descricao" value="{{old('descricao',$vinculo->descricao)}}" type="text" class="form-control" placeholder="Digite o nome do vínculo" required />
            </div>
        </div>

        <div class="row">
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('vinculo.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

    </form>
    @endsection
