@extends('layouts.template')
@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <h1>
                Gerenciamento de usuários
            </h1>
        </div>

        <div class="col-12 col-md-10 my-3">
            <a href="{{route('usuario.index')}}" class="btn btn-danger btn-sm">
                Voltar
            </a>
        </div>

        <div class="col-8 shadow p-3 ">
            <h4 class="font-weight-bold">
                Novo usuário
            </h4>
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                @endforeach
                </div>
            @endif
            <form action="{{route('usuario.update',$user)}}" method="post" class="form-row">
            @csrf
            @method('PUT')
                <div class="form-group col-4">
                  <label class="font-weight-bold">Nome</label>
                  <input type="text" name="name" id="name" class="form-control" value="{{old('name',$user->name)}}" required>
                </div>
                <div class="form-group col-4">
                  <label class="font-weight-bold">Email</label>
                  <input type="email" name="email" id="email" class="form-control" value="{{old('email',$user->email)}}" required>
                </div>
                <div class="form-group col-4">
                  <label class="font-weight-bold">Unidade de Saúde</label>
                  <select name="unidade_id" id="unidade" class="form-control" required>
                  <option value="" disabled>Selecione uma unidade de saúde</option>
                  @foreach($unidades as $u)
                    <option value="{{$u->id}}" {{$u->id == $user->unidade_id?"selected":""}}>
                        {{$u->nome}}
                    </option>
                  @endforeach
                  </select>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">
                        Salvar
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection