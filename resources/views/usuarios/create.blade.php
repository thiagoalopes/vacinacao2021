@extends('layouts.template')
@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <h1>
                Gerenciamento de usuários
            </h1>
        </div>

        <div class="col-12 col-md-10 my-3">
            <a href="{{route('usuario.index')}}" class="btn btn-danger btn-sm">
                Voltar
            </a>
        </div>

        <div class="col-8 shadow p-3 ">
            <h4>
                Novo usuário
            </h4>
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                @foreach($errors->all() as $e)
                    <p>{{$e}}</p>
                @endforeach
                </div>
            @endif
            <form action="{{route('usuario.store')}}" method="post" class="form-row">
            @csrf
                <div class="form-group col-4">
                  <label for="">Nome</label>
                  <input type="text" name="nome" id="nome" class="form-control" required>
                </div>
                <div class="form-group col-4">
                  <label for="">Email</label>
                  <input type="email" name="email" id="email" class="form-control" required>
                </div>
                <div class="form-group col-4">
                  <label for="">Unidade de Saúde</label>
                  <select name="unidade" id="unidade" class="form-control" required>
                  @foreach($unidades as $u)
                    <option value="{{$u->id}}">{{$u->nome}}</option>
                  @endforeach
                  </select>
                </div>
                <div class="form-group col-4 offset-4">
                    <div class="form-group">
                      <label for="">Perfil</label>
                      <select class="form-control" name="perfil" id="perfil">
                        @if(auth()->user()->perfil == 1)
                        <option value="2">Administrador de unidade</option>
                        @endif
                        <option value="3">Operador</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>
@endsection