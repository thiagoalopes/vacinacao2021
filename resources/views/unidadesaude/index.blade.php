@extends('layouts.template')

@section('content')

@include('messages.alert')

<div class="text-center col-12 mt-5">
  <a href="{{route('unidadesaude.create')}}" class="btn btn-primary btn-small">
    <b>Nova Unidade de Saúde</b>
  </a>
</div>

<div class="col-12 text-center mt-3">
  <h1>
    Listagem de Unidades
  </h1>
</div>

<table class="table table-bordered shadow" id="unidadeTable">
  <thead>
    <tr class="text-center">
      <th>CNES</th>
      <th>Nome</th>
      <th>Município</th>
      <th>Ações</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($unidadesaude as $u)
    <tr class="text-center">
      <td>{{$u->cnes}}</td>
      <td>{{strtoupper($u->nome)}}</td>
      <td>{{strtoupper($u->municipio->nome)}}</td>
      <td>
        <div class="dropdown open">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
            Selecione
          </button>
          <div class="dropdown-menu" aria-labelledby="triggerId">
            <a class="dropdown-item" href="{{route('unidadesaude.edit', $u->id)}}">Editar</a>
            {{-- <a class="dropdown-item" onclick="return confirm('Confirmar exclusão')" href="{{url('unidadesaude/remove/'.$u->id)}}">Excluir</a> --}}
          </div>
      </td>
      </div>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection