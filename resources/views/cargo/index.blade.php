@extends('layouts.template')

@section('content')

@include('messages.alert')

<div class="text-center col-12 mt-5">
    <a href="{{route('cargo.create')}}" class="btn btn-primary btn-small">
        <b>Novo Cargo</b>
    </a>
</div>

<div class="col-12 text-center mt-3">
    <h1>
        Listagem de Cargos
    </h1>
</div>
<table class="table table-bordered shadow col-12" id="cargoTable">
    <thead>
        <tr class="text-center">
            <th>Nome</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($cargo as $c)
        <tr class="text-center">
            <td>{{$c->descricao}}</td>

            <th>
                <div class="dropdown open">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                        Selecione
                    </button>
                    <div class="dropdown-menu" aria-labelledby="triggerId">
                        <a class="dropdown-item" href="{{route('cargo.edit', $c)}}">Editar</a>
                        <a class="dropdown-item" onclick="return confirm('Confirmar exclusão')" href="{{url('cargo/remove/'.$c->id)}}">Excluir</a>
                    </div>

                </div>
            </th>
        </tr>
        @endforeach
    </tbody>
</table>
</div>

<script>
    $('#cargoTable').DataTable({
        "searching": true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
        },
        "columnDefs": [{
            "targets": [-1],
            "orderable": false
        },
        ],
    });
</script>
@endsection
