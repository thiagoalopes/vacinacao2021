@extends('layouts.template')

@section('content')


@include('messages.alert')

@if($cargo->exists)

<div class="col-12 text-center">
    <h3>EDITAR CARGO</h3>
</div>

<form action="{{route('cargo.update', $cargo)}}" method="post">
    <input type="hidden" name="_method" value="put">

    @else

    <div class="col-12 text-center">
        <h3>CADASTRO DE CARGO</h3>
    </div>

    <form action="{{route('cargo.store')}}" method="post">
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-12">
                <label>Nome *</label>
                <input name="descricao" id="descricao" value="{{old('descricao',$cargo->descricao)}}" type="text" class="form-control" placeholder="Digite o nome do cargo" required />
            </div>
        </div>

        <div class="row">
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('cargo.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

    </form>
    @endsection
