@extends('layouts.template')
@push('style')
<style>
    body {
        background-color: #005C83;
        font-family: Verdana, sans-serif;
        font-size: 1.1rem;
    }
</style>
@endpush

@section('content')
<!-- <div class="container"> -->
<div class="row justify-content-center">
    <div class="col-12 col-md-6">
        <form method="POST" action="{{ route('login') }}" style="margin-top:15%" class="text-white">
            @csrf

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right  font-weight-bold">
                    Informe seu email
                </label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right font-weight-bold">
                    Digite sua senha
                </label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn " style="background-color: #ef9d38; border: 1px solid #ef9d38;color: #132152">
                        Entrar
                    </button>

                    @if (Route::has('password.request'))
                    <a class="btn btn-link text-white" href="{{ route('password.request') }}">
                        Esqueci minha senha
                    </a>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>
<!-- </div> -->
@endsection
