@extends('layouts.template')

@section('content')


@include('messages.alert')

@if($comorbidade->exists)

<div class="col-12 text-center">
    <h3>EDITAR COMORBIDADE</h3>
</div>

<form action="{{route('comorbidade.update', $comorbidade)}}" method="post">
    <input type="hidden" name="_method" value="put">

    @else

    <div class="col-12 text-center">
        <h3>CADASTRO DE COMORBIDADE</h3>
    </div>

    <form action="{{route('comorbidade.store')}}" method="post">
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="form-group col-12">
                <label>Nome *</label>
                <input name="descricao" id="descricao" value="{{old('descricao',$comorbidade->descricao)}}" type="text" class="form-control" placeholder="Digite o nome da comorbidade" required />
            </div>
        </div>

        <div class="row">
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('comorbidade.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

    </form>
    @endsection
