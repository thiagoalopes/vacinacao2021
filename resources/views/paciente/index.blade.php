@extends('layouts.template')

@section('content')

@include('messages.alert')

<div class="text-center col-12 mt-5">
    <a href="{{route('paciente.create')}}" class="btn btn-primary btn-small">
        <b>Novo paciente</b>
    </a>
</div>

<div class="col-12 text-center mt-3">
    <h1>
        Listagem de Pacientes
    </h1>
</div>
<table class="table table-bordered shadow col-12" id="pacienteTable">
    <thead>
        <tr class="text-center">
            <th>Nome</th>
            <th>Cpf</th>
            <th>CPF</th>{{--cpf para pesquisa sem ponto--}}
            <th>Data de Nascimento</th>
            <th>Raça/Cor</th>
            <th>Idade</th>
            <th>Fase Vacinação</th>
            <th>Profissional da Saúde?</th>
            <th>Unidade de Cadastro</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tfoot id="footPaciente">
    <tr class="text-center">
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    </tfoot>
    <tbody>
        @foreach ($pacientes as $p)
        <tr class="text-center">
            <td>{{$p->nome}}</td>
            <td>{{App\Utils\StringsUtil::formatar_cpf_oculto($p->cpf)}}</td>
            <td>{{$p->cpf}}</td>
            <td>{{$p->DataNascimento()}}</td>
            <td>{{$p->raca?$p->raca->descricao:''}}</td>
            <td>{{$p->getIdade($p->data_nascimento)}}</td>
            <td>{{$p->fase_vacinacao}}</td>
            <td>{{$p->getprofissionalsaude()}}</td>
            <td>{{$p->unidade_cadastro->nome}}</td>
            <th>

             @if(auth()->user()->unidade_id == $p->unidade_cadastro->id)
                <div class="dropdown open">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                        Selecionar
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('paciente.edit',$p)}}">
                            Editar
                        </a>
                        <a class="dropdown-item" target="new" href="{{route('visualizacao',$p->id)}}">Visualizar</a>
                    </div>
                </div>
              @endif
            </th>
        </tr>
        @endforeach
    </tbody>

</table>
</div>

<script>
    $('#pacienteTable').DataTable({
        "searching": true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
        },
        //Pesquisa individual no grid
        initComplete: function () {

            //Pesquisa individual com input
            this.api().columns([6]).every(function () {
                var column = this;
                var input = document.createElement("input");
                input.setAttribute("id", "idInputSearchStatus");
                input.placeholder = "Pesquisar Fase";
                input.placeholder.size = '5px';
                //input.title = "Pressione Enter após a pesquisa.";
                input.size = '11';
                //  $(input).appendTo($(column.footer()).empty())
                $(input).appendTo($(column.footer()))
                    .on('keyup change', function () {
                        column.search($(this).val()).draw();
                    });
            });

        },
        "columnDefs": [{
                "targets": [-1],
                "orderable": false
            },
            {
                "targets": [2],
                "visible": false
            }
        ],
    });
</script>
@endsection
