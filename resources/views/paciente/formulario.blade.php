@extends('layouts.template')

@section('content')

@include('messages.alert')
@include('messages.error')

@if($paciente->exists)

<div class="col-12 text-center">
    <h3>EDITAR PACIENTE</h3>
</div>

<form action="{{route('paciente.update', $paciente)}}" method="post">
    <input type="hidden" name="_method" value="put">

    @else

    <div class="col-12 text-center">
        <h3>CADASTRO DE PACIENTE</h3>
    </div>

    <form action="{{route('paciente.store')}}" method="post" class="col-12">
        @endif
        {{ csrf_field() }}

        <input name="pacienteid" id="pacienteid" value="{{$paciente->id}}" type="hidden" />

        <div class="row">
            <div class="col-6">
                <label>Nome<span>*</span></label>
                <input name="nome" id="nome" placeholder="Nome Completo" class="form-control mb-3" value="{{old('nome',$paciente->nome)}}" required />
            </div>
            <div class="col-6">
                <label>CPF<span>*</span></label>
                <input name="cpf" id="cpf" class="form-control mb-3 cpf" value="{{old('cpf',$paciente->cpf)}}" required {{$paciente->id?'readonly':''}} />
            </div>
        </div> 

        <div class="row">
            <div class="col-4">
                <label>CNS</label>
                <input name="cns" id="cns" class="form-control mb-3 cns" value="{{old('cns',$paciente->cns)}}" />
            </div>
            <div class="col-4">
                <label>Raça/Cor</label>
                <select class="form-control" id="raca_id" name="raca_id">
                    <option value="" selected>---- Selecione ----</option>
                    @foreach($racas as $r)
                    <option value="{{$r->id}}" {{(old('raca_id', $paciente->raca_id) == $r->id?'selected':'')}}>{{$r->descricao}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-4">
                <label>Data de Nascimento<span>*</span></label>
                <input type="date" min="01" name="data_nascimento" id="data_nascimento" class="form-control mb-3" value="{{old('data_nascimento',$paciente->data_nascimento)}}" required />
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <label>Sexo<span>*</span> </label>
                <select class="form-control mb-3" name="sexo" id="sexo" required>
                    <option value="" selected>-- Selecione --</option>
                    <option value="1" {{(old('sexo', $paciente->sexo) == '1'?'selected':'')}}>Masculino</option>
                    <option value="2" {{(old('sexo', $paciente->sexo) == '2'?'selected':'')}}>Feminino</option>
                </select>
            </div>
            <div class="col-4">
                <label>Fase vacinação<span>*</span> </label>
                <select class="form-control mb-3" name="fase_vacinacao" id="fase_vacinacao" required>
                    <option value="" selected>-- Selecione --</option>
                    <option value="1" {{(old('fase_vacinacao', $paciente->fase_vacinacao) == '1'?'selected':'')}}>1</option>
                    <option value="2" {{(old('fase_vacinacao', $paciente->fase_vacinacao) == '2' ?'selected':'')}}>2</option>
                    <option value="3" {{(old('fase_vacinacao', $paciente->fase_vacinacao) == '3' ?'selected':'')}}>3</option>
                    <option value="4" {{(old('fase_vacinacao', $paciente->fase_vacinacao) == '4' ?'selected':'')}}>4</option>
                </select>
            </div>
            <div class="col-4">
                <label>Município Residência</label>
                <select class="form-control mb-3" name="municipio_id" id="municipio_id">
                    <option value="" selected>-- Selecione --</option>
                    @foreach ($municipios as $m)
                    <option value="{{$m->id}}" {{(old('municipio_id', $paciente->municipio_id) == $m->id?'selected':'')}}>{{$m->nome}}</option>
                    @endforeach
                </select>
            </div>
        </div>        

        <div class="row">
            <div class="col-6">
                <label>Comorbidade? </label>
                <select class="comorbidadeList form-control mb-3" id="comorbidade_id" name="comorbidades[]" multiple="multiple">
                <!-- $comorbidades -->
                <!-- $paciente->comorbidade -->
                @foreach($comorbidades as $c)
                    <option value="{{$c->id}}" {{$paciente->hasComorbidade($c->id)}}>
                        {{$c->descricao}}
                    </option>
                @endforeach
                </select>
            </div>

            <div class="col-6">
                Profissional de saúde?
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="ckProfSaude" id="ckProfSaude1" value="1" {{old('ckProfSaude', $paciente->profissional_saude) ==1? 'checked':''}}>
                    <label class="form-check-label" for="ckProfSaude1">
                        Sim
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="ckProfSaude" id="ckProfSaude2" value="0" {{old('ckProfSaude', $paciente->profissional_saude)==0? 'checked':''}}>
                    <label class="form-check-label" for="ckProfSaude2">
                        Não
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group" style="margin-top: 30px;display:none" id="divProfissional">

            @include('paciente.formulario_prof')

        </div>


        <div class="row mt-3"></div>
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('paciente.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

    </form>


    <script>
        $(document).ready(function() {
            
           
             $('.comorbidadeList').select2();


            //após carregar página, no caso de validação
            selecionaProfissional();

            //é profissional
            $('#ckProfSaude1').on('change', function() {
                selecionaProfissional();
            });

            //não é profissional
            $('#ckProfSaude2').on('change', function() {
                selecionaProfissional();

                var pacienteid = $('#pacienteid').val();

                if (pacienteid == "") {
                    $('#unidade_id').val("");
                    $('#vinculo_us').val("");

                    $('#setor_trabalho_id').val("");
                    $('#setor_atuacao').val("");
                    $('#cargo_id').val("");
                    $('#vinculo_id').val("");

                    $('#turno_id').val("");
                }

            });


            function selecionaProfissional() {

                var ckProfSaude = $('#ckProfSaude1').is(":checked");

                //exibir dados para preenchimento do profissional
                if (ckProfSaude) {
                    $('#divProfissional').show();

                } else {
                    $('#divProfissional').hide();
                }
            }



        });
    </script>
    @endsection