<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <title>Paciente {{$paciente->cpf}}</title>
    <style>
        @media print {
            .container {
                display: inline;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img class="col-12 img-fluid" src="{{asset('img/banner.png')}}">
                <h1 class="text-center my-2">
                     Paciente
                </h1>
                <button class="btn btn-primary d-print-none" onClick="window.print();">Imprimir relatório</button> <br class="mb-2">
            </div>
            <div class="col-12 px-5">
                <h3>Dados do paciente</h3>
                <div class="card">
                    <div class="row p-4">
                        <div class="col-12"><b>Nome do paciente:</b>  {{$paciente->nome}}</div>
                        <div class="col-4"><b>Sexo:</b>  {{$paciente->getsexo()}}</div>
                        <div class="col-4"><b>CPF:</b>  {{App\Utils\StringsUtil::formatar_cpf_oculto($paciente->cpf)}}</div>
                        <div class="col-4"><b>Data de nascimento:</b>  {{$paciente->dataNascimento()}}</div>
                        <div class="col-4"><b>Idade:</b>  {{$paciente->getIdade($paciente->data_nascimento)}}</div>

                        <div class="col-4"><b>Raça/cor:</b>  {{$paciente->raca?$paciente->raca->descricao:''}}</div>

                      {{--  <div class="col-4"><b>Status:</b> {{$paciente->status()}}</div>--}}
                        <div class="col-12">
                            <b>Município Residência: </b>
                            {{$paciente->municipio ? $paciente->municipio->nome . '/' . $paciente->municipio->estado->uf : ''}}
                        </div>

                        <div class="col-12">
                            <b>Unidade de saúde de cadastro: </b>
                            {{$paciente->unidade_cadastro->nome}} ({{$paciente->unidade_cadastro->municipio->nome}} / {{$paciente->unidade_cadastro->municipio->estado->uf}})
                        </div>

                        <div class="col-12">
                            <b>Fase Vacinação: </b>
                            {{$paciente->fase_vacinacao}}
                        </div>

                    </div>
                </div>
            </div>

            @if($paciente->comorbidade->count()>0)
                <div class="col-12 my-2 px-5">
                    <h3>Comorbidades</h3>
                    <ul class="list-group">
                        <li class="list-group-item">
                        @foreach($paciente->comorbidade->sortBy('descricao') as $b)

                            {{$b->descricao}}<br>

                        @endforeach
                        </li>
                    </ul>
                </div>
            @endif


            @if($paciente->pacienteunidade)
            <div class="col-12 my-2 px-5">
                <h3>Dados do Profissional</h3>

                <li class="list-group-item">
                    <ul>
                        <li>
                            <b>Unidade: </b>   {{$paciente->pacienteunidade->unidade->nome}}
                        </li>
                        <li>
                            <b>Vínculo: </b>   {{$paciente->pacienteunidade->vinculo?$paciente->pacienteunidade->vinculo->descricao:''}}
                        </li>
                        <li>
                            <b>Turno: </b>   {{$paciente->pacienteunidade->turno?$paciente->pacienteunidade->turno->descricao:''}}
                        </li>
                        <li>
                            <b>Setor de trabalho: </b>   {{$paciente->pacienteunidade->setortrabalho->descricao}}
                        </li>
                        <li>
                            <b>Setor de atuação: </b>   {{$paciente->pacienteunidade->setor_atuacao}}
                        </li>
                        <li>
                            <b>Cargo: </b>   {{$paciente->pacienteunidade->cargo?$paciente->pacienteunidade->cargo->descricao:''}}
                        </li>
                        <li>
                            <b>Vínculo com outros estabelecimentos? </b>   {{$paciente->getvinculous()}}
                        </li>

                    </ul>
                </li>

            </div>
            @endif


        </div></div>


</body>
</html>
