@extends('layouts.template')
@section('content')
<div class="col-12">
    <h1>
        Primeiro acesso ao sistema
    </h1>
    <div class="alert alert-info" role="alert">
        <p class="text-dark font-weight-bold text-center p-2">
            Bem-vindo(a) {{auth()->user()->name}} <br>
            Este é seu primeiro acesso ao sistema, por razões de segurança, altere sua senha inicial para uma senha pessoal
        </p>
    </div>
</div>
<div class="col-12">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{route('primeiroacesso.update')}}" method="post" class="form-row">
    @csrf
        <div class="form-group col-6">
          <label class="font-weight-bold">Nova senha</label>
          <input type="password" name="password" id="password" class="form-control" required>
        </div>
        <div class="form-group col-6">
          <label class="font-weight-bold">Repita sua nova senha</label>
          <input type="password" name="password_confirmation" id="password-confirm" class="form-control" required>
          <small id="helpId" class="text-muted">
            A senha deve ser igual a informada no primeiro campo "Nova senha"
          </small>
        </div>
        <div class="col-12 text-center">
            <button type="submit" class="btn btn-primary">Cadastrar nova senha</button>
        </div>
    </form>
</div>
@endsection