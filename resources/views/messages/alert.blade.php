<script>
    window.setTimeout(function() {
        $(".withtimeout").fadeTo(400, 0).slideUp(400, function(){
            $(this).remove();
        });
    }, 4000);
</script>

	<div>
        @if(Session::has('status'))
                    <div class="alert alert-success withtimeout ">
                        <a href="#" class="close" data-dismiss="alert">&times</a>
                        <strong> Sucesso!  </strong> {{ Session::get('status')  }}
                    </div>
        @elseif(Session::has('status-not'))
        <div class="alert alert-danger withtimeout ">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong> Erro!</strong> {{ Session::get('status-not') }}
                    </div>
            @elseif(Session::has('status-not2'))
            <div class="alert alert-danger withtimeout">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
               {{ Session::get('status-not2') }}
            </div>
        @elseif(Session::has('status-alert'))
            <div class="alert alert-warning withtimeout">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        {{ Session::get('status-alert') }}
             </div>
        @endif
    </div>
