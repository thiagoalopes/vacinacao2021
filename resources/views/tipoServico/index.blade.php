@extends('layouts.template')

@section('content')

@include('messages.alert')

<div class="text-center col-12 mt-5">
    <a href="{{route('tipoServico.create')}}" class="btn btn-primary btn-small">
        <b>Novo tipo de Serviço</b>
    </a>
</div>

<div class="col-12 text-center mt-3">
    <h1>
        Listagem de Tipos de Serviço
    </h1>
</div>
<table class="table table-bordered shadow col-12" id="tipoServicoTable">
    <thead>
        <tr class="text-center">
            <th>Nome</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($tipoServico as $t)
        <tr class="text-center">
            <td>{{$t->descricao}}</td>

            <th>
                <div class="dropdown open">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                        Selecione
                    </button>
                    <div class="dropdown-menu" aria-labelledby="triggerId">
                        <a class="dropdown-item" href="{{route('tipoServico.edit', $t)}}">Editar</a>
                        <a class="dropdown-item" onclick="return confirm('Confirmar exclusão')" href="{{url('tipo-servico/remove/'.$t->id)}}">Excluir</a>
                    </div>

                </div>
            </th>
        </tr>
        @endforeach
    </tbody>
</table>
</div>

<script>
    $('#tipoServicoTable').DataTable({
        "searching": true,
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
        }
    });
</script>
@endsection