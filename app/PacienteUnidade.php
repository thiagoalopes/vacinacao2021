<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteUnidade extends Model
{
    protected $table = 'paciente_unidade';

    public $fillable = [
        'id','paciente_id','unidade_id', 'setor_atuacao', 'setor_trabalho_id', 'cargo_id', 'vinculo_id',
        'turno_id'
    ];

    public function unidade(){
        return $this->belongsTo('App\UnidadeSaude');
    }

    public function setorTrabalho(){
        return $this->belongsTo('App\SetorTrabalho');
    }

    public function cargo(){
        return $this->belongsTo('App\Cargo');
    }

    public function vinculo(){
        return $this->belongsTo('App\Vinculo');
    }

    public function turno(){
        return $this->belongsTo('App\Turno');
    }
}
