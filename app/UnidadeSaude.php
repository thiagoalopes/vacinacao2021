<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadeSaude extends Model
{
    protected $table = 'unidades';

    protected $fillable = [
        'nome', 'cnes', 'tipo_prestacao_id','tipo_servico_id',
        'referencia_covid','municipio_id'
    ];

    public function municipio(){
        return $this->belongsTo('App\Municipio');
    }
}
