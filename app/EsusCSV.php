<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class EsusCSV extends Model
{
    protected $table = 'esuscsv';

    public static function totalVacinados()
    {
        $totalVacinados = EsusCSV::all()->count();
        return $totalVacinados;
    }

    public static function totalVacinadosHoje()
    {
        $totalVacinadosHoje = EsusCSV::where('vacina_dataAplicacao', Carbon::now())->count();
        return $totalVacinadosHoje;
    }


    public static function totalEstabelecimentos()
    {
        $totalEstabelecimentos = EsusCSV::distinct('estalecimento_noFantasia')->count();
        return $totalEstabelecimentos;
    }


    public static function totalVacinadores()
    {
        $totalEstabelecimentos = EsusCSV::distinct('vacina_profissionalAplicador_cpf')->count();
        return $totalEstabelecimentos;
    }

    public static function totalSubCategorias($vacina_categoria_codigo = 9)
    {
        $totalGrupoAtendimento = DB::select(
            "
        SELECT
            t.grupo_atendimento AS grupo_atendimento,
            SUM(t.quantidade) AS quantidade
        FROM
            (SELECT
                COUNT(ee.vacina_grupoAtendimento_codigo) AS quantidade,
                    ee.vacina_grupoAtendimento_nome AS grupo_atendimento
            FROM
                esuscsv ee
            WHERE
                (ee.vacina_categoria_codigo = {$vacina_categoria_codigo})
            GROUP BY ee.vacina_grupoAtendimento_nome) t
        GROUP BY t.grupo_atendimento"
        );

        return $totalGrupoAtendimento;
    }

    public static function totalCategorias()
    {
        $totalCategorias = EsusCSV::selectRaw(
            'vacina_categoria_nome categoria,
            vacina_categoria_codigo codigo,
            count(vacina_categoria_codigo) quantidade')
            ->groupBy('vacina_categoria_nome')
            ->groupBy('vacina_categoria_codigo')
            ->get();
        return $totalCategorias;
    }

    public static function totalVacinadosporDia()
    {
        $totalVacinadosporDia = EsusCSV::selectRaw(
            'DATE_FORMAT(CAST(vacina_dataAplicacao AS DATE), "%d/%m/%Y") data_aplicacao, DATE_FORMAT(CAST(vacina_dataAplicacao AS DATE), "%Y/%m/%d") date, count(vacina_dataAplicacao) quantidade')
            ->groupBy(DB::raw('DATE_FORMAT(CAST(vacina_dataAplicacao AS DATE), "%d/%m/%Y")'))
            ->groupBy(DB::raw('DATE_FORMAT(CAST(vacina_dataAplicacao AS DATE), "%Y/%m/%d")'))
            ->orderBy('date', 'asc')
            ->get();
        return $totalVacinadosporDia;
    }

    public static function totalVacinadosPorLocal()
    {
        $totalVacinadosPorLocal = EsusCSV::selectRaw(
            'estalecimento_noFantasia estabelecimento,
            count(estalecimento_noFantasia) quantidade')
            ->groupBy('estalecimento_noFantasia')
            ->orderBy('quantidade', 'desc')
            ->get();
        return $totalVacinadosPorLocal;
    }


}
