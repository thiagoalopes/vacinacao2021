<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPrestacao extends Model
{
    protected $table = 'tipo_prestacao';
}
