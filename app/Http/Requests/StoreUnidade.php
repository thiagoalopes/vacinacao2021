<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUnidade extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'cnes' => 'required|unique:unidades,cnes|max:255',
            'referencia_covid' => 'required',
            'tipo_prestacao_id' => 'required',
            'tipo_servico_id' => 'required',
            'municipio_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nome.required' => 'O campo Nome é de preenchimento obrigatório!',
            'cnes.unique' => 'Unidade já cadastrada',
            'referencia_covid.required' => 'O campo Referencia Covid é de preenchimento obrigatório!',
            'tipo_prestacao_id.required' => 'O campo Tipo de Prestação é de preenchimento obrigatório!',
            'tipo_servico_id.required' => 'O campo Tipo de Serviço é de preenchimento obrigatório!',
            'municipio_id.required' => 'O campo Municipio é de preenchimento obrigatório!',
        ];
    }
}
