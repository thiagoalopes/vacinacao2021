<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
// use Illuminate\Validation\Validator;

class UpdatePrimeiroAcesso extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=>'required|confirmed|min:6',
        ];
    }
    public function messages()
    {
        return [
            'password.min'=>'A senha deve conter no mínimo 6 digítos',
            'password.confirmed'=>"Ambas senhas dever ser idênticas"
        ];
    }
}
