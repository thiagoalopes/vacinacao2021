<?php

namespace App\Http\Controllers\Api\v1;

use App\EsusCSV;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardApi extends Controller
{

    public function __construct()
    {
        $this->middleware(['client']);
    }

    public function index()
    {

        //Json response
        $response = [
            'totalValcinados'=>EsusCSV::totalVacinados(),
            'totalVacinadosHoje'=>EsusCSV::totalVacinadosHoje(),
            'totalEstabelecimentos'=>EsusCSV::totalEstabelecimentos(),
            'totalVacinadores'=>EsusCSV::totalVacinadores(),
            'totalVacinadosPorLocal'=>EsusCSV::totalVacinadosPorLocal(),
            'totalVacinadosporDia'=>EsusCSV::totalVacinadosporDia(),
            'totalCategorias'=>EsusCSV::totalCategorias(),
            'totalSubcategorias'=>[]
        ];

        return response()->json($response);
    }


    public function filterTotalSubcategorias(Request $request)
    {
        $valid = $request->validate([
            'codCategoria'=>'required|regex:/^[0-9]+$/'
        ]);

        //Json response
        $response = [
            'totalSubcategorias'=>EsusCSV::totalSubcategorias($valid['codCategoria'])
        ];

        return response()->json($response);
    }
}
