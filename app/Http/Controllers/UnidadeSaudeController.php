<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUnidade;
use App\Municipio;
use App\TipoPrestacao;
use App\TipoServico;
use App\UnidadeSaude;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Request as rq;
use Illuminate\Support\Facades\DB;

class UnidadeSaudeController extends Controller
{
    public function __construct(){
        $this->middleware('verificaperfil:unidades');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unidadesaude = UnidadeSaude::orderBy('nome')->get();

        return view('unidadesaude.index', compact('unidadesaude'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidadesaude = new UnidadeSaude();
        $tipo_prestacao = TipoPrestacao::orderBy('descricao')->get();
        $tipo_servico = TipoServico::orderBy('descricao')->get();
        $municipio_id = Municipio::where('estado_id', 3)->get();
        return view('unidadesaude.formulario', compact('unidadesaude','tipo_prestacao','tipo_servico','municipio_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnidade $request)
    {
        $unidadesaude = new UnidadeSaude();
        $unidadesaude->nome = $request->nome;
        $unidadesaude->cnes = $request->cnes;
        $unidadesaude->referencia_covid = $request->referencia_covid;
        $unidadesaude->tipo_prestacao_id = $request->tipo_prestacao_id;
        $unidadesaude->tipo_servico_id = $request->tipo_servico_id;
        $unidadesaude->municipio_id = $request->municipio_id;
        
                
            try{
                $save = $unidadesaude->save();
                if($save){
                    rq::session()->flash('status','Unidade cadastrada com sucesso');
                }else{
                    rq::session()->flash('status-not', 'Ocorreu um erro ao salvar a unidade');
                }
            }catch(\Exception $e){
                rq::session()->flash('status-not','Ocorreu um erro ao salvar a unidade'.$e->getMessage());
            }
            Log::channel('relatoria')->info("Unidade Cadastrada",[
                'user_id'=>auth()->user()->id,
                'dados'=>$unidadesaude
            ]);
            return redirect()->route('unidadesaude.index');
    }
        
        
        // $unidadesaude = Unidadesaude::create([
        //     'nome' => $request->nome,
        //     'cnes' => $request->cnes,
        //     'referencia_covid' => $request->referencia_covid,
        //     'tipo_prestacao_id' => $request->tipo_prestacao_id,
        //     'tipo_servico_id' => $request->tipo_servico_id,
        //     'municipio_id' => $request->municipio_id
        // ]);
        //validacao request
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UnidadeSaude $unidadesaude)
    {
        $tipo_prestacao = TipoPrestacao::orderBy('descricao')->get();
        $tipo_servico = TipoServico::orderBy('descricao')->get();
        $municipio_id = Municipio::where('estado_id', 3)->get();
        return view('unidadesaude.formulario', compact('unidadesaude','tipo_prestacao','tipo_servico','municipio_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnidadeSaude $unidadesaude)
    {
        $unidadesaude->nome = $request->nome;
        $unidadesaude->cnes = $request->cnes;
        $unidadesaude->referencia_covid = $request->referencia_covid;
        $unidadesaude->tipo_prestacao_id = $request->tipo_prestacao_id;
        $unidadesaude->tipo_servico_id = $request->tipo_servico_id;
        $unidadesaude->municipio_id = $request->municipio_id;

        $existeCNES = DB::table('unidades')->where([           
            ['cnes', '=', $unidadesaude->cnes],
            ['id', '<>', $unidadesaude->id],
        ])->count();

        if($existeCNES > 0){        
            rq::session()->flash('status-not', 'Já existe unidade com CNES cadastrado.');
            return redirect()->route('unidadesaude.index');
        }


        try {
            $save = $unidadesaude->save();

            if($save){
                rq::session()->flash('status', 'Unidade atualizada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar unidade.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar unidade.'.$e->getMessage());
        }
        Log::channel('relatoria')->info("Unidade Alterada",[
            'user_id'=>auth()->user()->id,
            'dados'=>$unidadesaude
        ]);
        return redirect()->route('unidadesaude.index');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $unidadesaude = UnidadeSaude::findOrFail($id);
        $unidadesaude->delete();
    }
}
