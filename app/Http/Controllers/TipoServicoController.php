<?php

namespace App\Http\Controllers;

use App\TipoServico;
use Illuminate\Http\Request;
use Request as rq;

class TipoServicoController extends Controller
{
    public function __construct(){
        $this->middleware('verificaperfil:tipo_servicos');
    }
    public function index(){

        $tipoServico = TipoServico::orderBy('descricao')->get();

        return view('tipoServico.index', compact('tipoServico'));
    }

    public function create(){

        $tipoServico = new TipoServico();

        return view('tipoServico.formulario', compact('tipoServico'));
    }

    public function store(Request $request){

        $tipoServico = new TipoServico();
        $tipoServico->descricao = $request->descricao;

        try {
            $save = $tipoServico->save();

            if($save){
                rq::session()->flash('status', 'Tipo de Serviço cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de serviço.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de serviço.'.$e->getMessage());
        }

        return redirect()->route('tipoServico.index');
    }

    public function edit(TipoServico $tipoServico){

        return view('tipoServico.formulario', compact('tipoServico'));
    }

    public function update(Request $request, TipoServico $tipoServico){

        $tipoServico->descricao = $request->descricao;

        try {
            $save = $tipoServico->save();

            if($save){
                rq::session()->flash('status', 'Tipo de serviço atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de serviço.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de serviço.'.$e->getMessage());
        }

        return redirect()->route('tipoServico.index');
    }

    public function delete($id)
    {
        $tipoServico = TipoServico::findOrFail($id);

        try{
            $tipoServico->delete();
            rq::session()->flash('status', 'Tipo de serviço '.$tipoServico->descricao.' excluído com sucesso.');
            return redirect()->route('tipoServico.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Tipo de Serviço não foi excluído. O mesmo está sendo utilizado em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Tipo de Serviço não foi excluído! '.$e->getMessage());
            }

            return redirect()->route('tipoServico.index');
        }

    }
}
