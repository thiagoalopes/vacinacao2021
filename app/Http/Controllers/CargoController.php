<?php

namespace App\Http\Controllers;

use App\Cargo;
use Illuminate\Http\Request;
use Request as rq;

class CargoController extends Controller
{
    public function __construct(){
        $this->middleware('verificaperfil:cargos');
    }
    public function index(){

        $cargo = Cargo::orderBy('descricao')->get();

        return view('cargo.index', compact('cargo'));
    }

    public function create(){

        $cargo = new Cargo();

        return view('cargo.formulario', compact('cargo'));
    }

    public function store(Request $request){

        $cargo = new Cargo();
        $cargo->descricao = $request->descricao;

        try {
            $save = $cargo->save();

            if($save){
                rq::session()->flash('status', 'Cargo cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar cargo.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar cargo.'.$e->getMessage());
        }

        return redirect()->route('cargo.index');
    }

    public function edit(Cargo $cargo){

        return view('cargo.formulario', compact('cargo'));
    }

    public function update(Request $request, Cargo $cargo){

        $cargo->descricao = $request->descricao;

        try {
            $save = $cargo->save();

            if($save){
                rq::session()->flash('status', 'Cargo atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar cargo.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar cargo.'.$e->getMessage());
        }

        return redirect()->route('cargo.index');
    }

    public function delete($id)
    {
        $cargo = Cargo::findOrFail($id);

        try{
            $cargo->delete();
            rq::session()->flash('status', 'Cargo '.$cargo->descricao.' excluído com sucesso.');
            return redirect()->route('cargo.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Cargo não foi excluído. O mesma está sendo utilizado em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Cargo não foi excluído! '.$e->getMessage());
            }

            return redirect()->route('cargo.index');
        }

    }
}
