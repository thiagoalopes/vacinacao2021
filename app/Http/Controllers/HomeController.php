<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePrimeiroAcesso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function primeiroacesso(){
        return view('primeiroacesso');
    }
    public function update(UpdatePrimeiroAcesso $request){
        $user  = auth()->user();
        $user->update([
            'password' => Hash::make($request->password),
            'first_access' => 0
        ]);
        return redirect()->route('home');
     }
}
