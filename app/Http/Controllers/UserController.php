<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUsuario;
use App\Http\Requests\UpdateUsuario;
use App\UnidadeSaude;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('verificaperfil:users');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('perfil','<>',1)->get();
        if(auth()->user()->perfil == 2)
            $users = User::where('unidade_id',auth()->user()->unidade->id)
                            ->where('perfil',3)            
                            ->get();
        return view('usuarios.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades = UnidadeSaude::all();
        if(auth()->user()->perfil == 2)
            $unidades = UnidadeSaude::where('id',auth()->user()->unidade->id)->get();
        // dd($unidades);
        return view('usuarios.create',compact('unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsuario $request)
    {
        try{
            $user = User::create([
                'name'=>$request->nome,
                'email'=>$request->email,
                'unidade_id'=>$request->unidade,
                'perfil'=>$request->perfil,
                'first_access'=>1,
                'password'=>Hash::make('123456'), // TODO: Ajustar no envio de email
            ]);

        }catch(Exception $e){
            return redirect()->route('usuario.index')->with('errors',"Não foi possível cadastrar usuário");

        }
        Log::channel('relatoria')->info("Usuário cadastrado",[
            'user_id'=>auth()->user()->id,
            'dados_usuario'=>$user
        ]);
        return redirect()->route('usuario.index')->with('success',"Usuário cadastrado com sucesso");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $unidades = UnidadeSaude::all();
        if(auth()->user()->perfil == 2)
            $unidades = UnidadeSaude::where('id',auth()->user()->unidade->id)->get();
        return view('usuarios.edit',compact('user','unidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsuario $request, User $usuario)
    {
        // dd($request->validated(),$usuario);
        try{
            $old = $usuario;
            $usuario->update($request->validated());
        }catch(Exception $e){
            return redirect()->route('usuario.index')->withErrors("Não foi possível editar usuário");
        }
        Log::channel('relatoria')->info("Usuário alterado",[
            'user_id'=>auth()->user()->id,
            'dados_usuario'=>$old,
            'alterações'=>$request->validated()
        ]);
        return redirect()->route('usuario.index')->with('success',"Dados do usuário alterado com sucesso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
