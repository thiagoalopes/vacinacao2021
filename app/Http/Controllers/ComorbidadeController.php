<?php

namespace App\Http\Controllers;

use App\Comorbidade;
use Illuminate\Http\Request;
use Request as rq;

class ComorbidadeController extends Controller
{
    public function __construct(){
        $this->middleware('verificaperfil:comorbidades');
    }
    public function index(){

        $comorbidade = Comorbidade::orderBy('descricao')->get();

        return view('comorbidade.index', compact('comorbidade'));
    }

    public function create(){

        $comorbidade = new Comorbidade();

        return view('comorbidade.formulario', compact('comorbidade'));
    }

    public function store(Request $request){

        $comorbidade = new Comorbidade();
        $comorbidade->descricao = $request->descricao;

        try {
            $save = $comorbidade->save();

            if($save){
                rq::session()->flash('status', 'Comorbidade cadastrada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar comorbidade.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar comorbidade.'.$e->getMessage());
        }

        return redirect()->route('comorbidade.index');
    }

    public function edit(Comorbidade $comorbidade){

        return view('comorbidade.formulario', compact('comorbidade'));
    }

    public function update(Request $request, Comorbidade $comorbidade){

        $comorbidade->descricao = $request->descricao;

        try {
            $save = $comorbidade->save();

            if($save){
                rq::session()->flash('status', 'Comorbidade atualizada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar comorbidade.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar comorbidade.'.$e->getMessage());
        }

        return redirect()->route('comorbidade.index');
    }

    public function delete($id)
    {
        $comorbidade = Comorbidade::findOrFail($id);

        try{
            $comorbidade->delete();
            rq::session()->flash('status', 'Comorbidade '.$comorbidade->descricao.' excluída com sucesso.');
            return redirect()->route('comorbidade.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Comorbidade não foi excluída. A mesma está sendo utilizada em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Comorbidade não foi excluída! '.$e->getMessage());
            }

            return redirect()->route('comorbidade.index');
        }

    }
}
