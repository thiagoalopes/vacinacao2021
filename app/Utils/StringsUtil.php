<?php
namespace App\Utils;

class StringsUtil {

    public static function limpaCPF_CNPJ($valor){

        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        return $valor;
    }

    public static function formatar_cpf_cnpj($doc) {
 
        $doc = preg_replace("/[^0-9]/", "", $doc);
        $qtd = strlen($doc);
 
        if($qtd >= 11) {
 
            if($qtd === 11 ) {
 
                $docFormatado = substr($doc, 0, 3) . '.' .
                                substr($doc, 3, 3) . '.' .
                                substr($doc, 6, 3) . '-' .
                                substr($doc, 9, 2);
            } else {
                $docFormatado = substr($doc, 0, 2) . '.' .
                                substr($doc, 2, 3) . '.' .
                                substr($doc, 5, 3) . '/' .
                                substr($doc, 8, 4) . '-' .
                                substr($doc, -2);
            }
 
            return $docFormatado;
 
        } else {
            return 'Documento inválido';
        }
    }

    public static function formatar_cpf_oculto($doc) {
 
        $doc = preg_replace("/[^0-9]/", "", $doc);
        $qtd = strlen($doc);      
 
        if($qtd >= 11) {           

            //pega os 4 últimos dígitos
            $form = substr( $doc, -2); 
            $form2 = substr( $doc, -5, -2); 
 
            $docFormatado = '***.***.'.$form2.'-'.$form;
 
            return $docFormatado;
 
        } else {
            return 'Documento inválido';
        }
    }

}