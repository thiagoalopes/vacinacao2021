<?php

use App\UnidadeSaude;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $us = UnidadeSaude::create([
            'nome'=>'Secretaria de Saúde',
            'cnes'=>'123456',
            'tipo_prestacao_id'=>1,
            'tipo_servico_id'=>1,
            'referencia_covid'=>0,
            'municipio_id'=>256
        ]);
        User::create([
            'name'=>'Master',
            'email'=>'master@gmail.com',
            'perfil'=>1,
            'unidade_id'=>$us->id,
            'first_access'=>0,
            'password'=> Hash::make('123456')
        ]);
    }
}
