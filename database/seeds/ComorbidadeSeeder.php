<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComorbidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comorbidade')->insert([
            'descricao' => 'Diabetes'
        ]);

        DB::table('comorbidade')->insert([
            'descricao' => 'Obesidade'
        ]);

        DB::table('comorbidade')->insert([
            'descricao' => 'Hipertensão Arterial'
        ]);

        DB::table('comorbidade')->insert([
            'descricao' => 'Tuberculose'
        ]);

        DB::table('comorbidade')->insert([
            'descricao' => 'Doença Pulmonar'
        ]);

        DB::table('comorbidade')->insert([
            'descricao' => 'Problemas Cardíacos'
        ]);
    }
}
