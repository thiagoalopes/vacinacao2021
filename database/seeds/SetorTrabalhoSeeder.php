<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SetorTrabalhoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setor_trabalho')->insert([
            'descricao' => 'UTI/semi-intensiva',
            'ordem_prioridade' => '1'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Sala Rosa',
            'ordem_prioridade' => '2'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Sala vermelha/emergência',
            'ordem_prioridade' => '3'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Enfermaria COVID',
            'ordem_prioridade' => '4'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Laboratório: coleta, processamento, análise',
            'ordem_prioridade' => '5'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Equipe de vacinação',
            'ordem_prioridade' => '6'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Acolhimento de sintomáticos respiratórios',
            'ordem_prioridade' => '7'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Remoção de pacientes COVID',
            'ordem_prioridade' => '8'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Serviços gerais/maqueiros',
            'ordem_prioridade' => '9'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Necrotério',
            'ordem_prioridade' => '10'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Distribuição de alimentos (nutrição)',
            'ordem_prioridade' => '11'           
        ]);

        DB::table('setor_trabalho')->insert([
            'descricao' => 'Vigilância epidemiológica/comissões hospitalares',
            'ordem_prioridade' => '12'           
        ]);

      
    }
}
