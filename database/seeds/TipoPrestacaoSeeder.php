<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPrestacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_prestacao')->insert([
            'descricao' => 'Hospital/Maternidade'         
        ]);

        DB::table('tipo_prestacao')->insert([
            'descricao' => 'Pública'         
        ]);

   
      
    }
}
