<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacienteUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_unidade', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('setor_atuacao');

            $table->bigInteger('setor_trabalho_id')->unsigned();
            $table->foreign('setor_trabalho_id')->references('id')->on('setor_trabalho');

            $table->bigInteger('paciente_id')->unsigned();
            $table->foreign('paciente_id')->references('id')->on('pacientes');

            $table->bigInteger('unidade_id')->unsigned();
            $table->foreign('unidade_id')->references('id')->on('unidades');

            $table->bigInteger('cargo_id')->unsigned()->nullable();
            $table->foreign('cargo_id')->references('id')->on('cargos');

            $table->bigInteger('vinculo_id')->unsigned()->nullable();
            $table->foreign('vinculo_id')->references('id')->on('vinculos');  
            
            $table->bigInteger('turno_id')->unsigned()->nullable();
            $table->foreign('turno_id')->references('id')->on('turnos');  

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente_unidade');
    }
}
